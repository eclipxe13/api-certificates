<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\CertificateDownloader;

interface HttpDownloaderInterface
{
    public function retrieve(string $url): string;
}

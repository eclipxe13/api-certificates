<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\CertificateDownloader;

class PhpStreamDownloader implements HttpDownloaderInterface
{
    public function retrieve(string $url): string
    {
        return file_get_contents($url);
    }

}

<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\CertificateDownloader;

use Psr\SimpleCache\CacheInterface;

class CertificateDownloader
{
    /** @var CacheInterface */
    private $cache;

    /** @var HttpDownloaderInterface */
    private $downloader;

    public function __construct(CacheInterface $cache, HttpDownloaderInterface $downloader)
    {
        $this->cache = $cache;
        $this->downloader = $downloader;
    }

    public function retrieve(string $serialNumber): string
    {
        $url = $this->buildCertificateUrl($serialNumber);

        if ($this->cache->has($url)) {
            return $this->cache->get($url);
        }

        $contents = $this->downloader->retrieve($url);
        $this->cache->set($url, $contents);
        return $contents;
    }

    public function buildCertificateUrl(string $serialNumber): string
    {
        return sprintf(
            'https://rdc.sat.gob.mx/rccf/%s/%s/%s/%s/%s/%s.cer',
            substr($serialNumber, 0, 6),
            substr($serialNumber, 6, 6),
            substr($serialNumber, 12, 2),
            substr($serialNumber, 14, 2),
            substr($serialNumber, 16, 2),
            $serialNumber
        );
    }
}

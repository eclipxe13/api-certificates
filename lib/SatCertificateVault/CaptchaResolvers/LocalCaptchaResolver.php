<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault\CaptchaResolvers;

use Eclipxe\ApiSatCertificates\SatCertificateVault\CaptchaResolverInterface;

class LocalCaptchaResolver implements CaptchaResolverInterface
{
    public function resolveImage(string $base64Image): string
    {
        return 'abcdef';
    }
}

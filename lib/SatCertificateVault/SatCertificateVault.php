<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;

class SatCertificateVault
{
    /** @var string */
    private $rfc;

    /** @var CaptchaResolverInterface */
    private $resolver;

    /** @var ScraperInterface */
    private $scraper;

    public function __construct(string $rfc, CaptchaResolverInterface $resolver, ScraperInterface $scraper = null)
    {
        $this->rfc = $rfc;
        $this->resolver = $resolver;
        $this->scraper = $scraper ?? new Scraper();
    }

    public function getScraper(): ScraperInterface
    {
        return $this->scraper;
    }

    public function getRfc(): string
    {
        return $this->rfc;
    }

    public function getResolver(): CaptchaResolverInterface
    {
        return $this->resolver;
    }

    public function obtainList(): CertificateList
    {
        $scraper = $this->getScraper();
        $searchPage = $scraper->obtainSearchPage();

        $imageUrl = $searchPage->extractCaptchaImageUrl();
        $image = $scraper->downloadImage($imageUrl);
        $captcha = $this->resolver->resolveImage($image->toBase64());

        $resultsPage = $scraper->obtainResultsPage($searchPage, $this->getRfc(), $captcha);

        return $resultsPage->extractCertificateList();
    }
}

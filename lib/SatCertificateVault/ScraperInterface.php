<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;

use RuntimeException;

interface ScraperInterface
{
    /**
     * GET search page contents
     *
     * @return SearchPage
     * @throws RuntimeException if unable to get search page
     */
    public function obtainSearchPage(): SearchPage;

    /**
     * Download the image url to a temporary file
     *
     * @param string $imageUrl
     * @return CaptchaImage
     */
    public function downloadImage(string $imageUrl): CaptchaImage;

    /**
     * Using the search page, fill the form with rfc and captcha and POST to result page
     *
     * @param SearchPage $page
     * @param string $rfc
     * @param string $captcha
     * @return ResultsPage
     */
    public function obtainResultsPage(SearchPage $page, string $rfc, string $captcha): ResultsPage;
}

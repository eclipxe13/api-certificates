<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;

interface CaptchaResolverInterface
{
    public function resolveImage(string $base64Image): string;
}

<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;

use Symfony\Component\DomCrawler\Crawler;

class ResultsPage
{
    /** @var Crawler */
    private $crawler;

    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    public function getContents(): string
    {
        return $this->crawler->html();
    }

    public function extractCertificateList(): CertificateList
    {
        return new CertificateList();
    }
}

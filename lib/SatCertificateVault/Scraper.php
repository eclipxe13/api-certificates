<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;
use Goutte\Client as Browser;
use RuntimeException;
use Throwable;


class Scraper implements ScraperInterface
{
    const URL_SEARCH_PAGE = 'https://portalsat.plataforma.sat.gob.mx/RecuperacionDeCertificados'
        . '/faces/consultaCertificados.xhtml';

    /** @var Browser */
    private $browser;

    public function __construct(Browser $browser = null)
    {
        $this->browser = $browser ?? new Browser();
    }

    public function obtainSearchPage(): SearchPage
    {
        try {
            $crawler = $this->browser->request('GET', self::URL_SEARCH_PAGE);
            $response = $this->browser->getInternalResponse();
            if (200 !== $response->getStatusCode()) {
                throw new RuntimeException("HTTP Status is not 200 OK {$response->getStatusCode()}");
            }
        } catch (Throwable $exception) {
            throw new RuntimeException('Unable to get search page', 0, $exception);
        }

        return new SearchPage($crawler);
    }

    public function downloadImage(string $imageUrl): CaptchaImage
    {
        try {
            $this->browser->request('GET', $imageUrl);
            $response = $this->browser->getInternalResponse();
            if (200 !== $response->getStatusCode()) {
                throw new RuntimeException("HTTP Status is not 200 OK {$response->getStatusCode()}");
            }
        } catch (Throwable $exception) {
            throw new RuntimeException('Unable to get captcha image', 0, $exception);
        }

        return new CaptchaImage($response->getContent());
    }

    public function obtainResultsPage(SearchPage $page, string $rfc, string $captcha): ResultsPage
    {
        try {
            $form = $page->extractForm();
            $crawler = $this->browser->submit($form, [
                'consultaCertificados:entradaRFC' => $rfc,
                'consultaCertificados:verCaptchaRFC' => $captcha,
            ]);
            $response = $this->browser->getInternalResponse();
            if (200 !== $response->getStatusCode()) {
                throw new RuntimeException("HTTP Status is not 200 OK {$response->getStatusCode()}");
            }
        } catch (Throwable $exception) {
            throw new RuntimeException('Unable to get results page', 0, $exception);
        }

        return new ResultsPage($crawler);
    }
}

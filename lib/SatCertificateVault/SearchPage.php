<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\SatCertificateVault;

use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Form;

class SearchPage
{
    /** @var Crawler */
    private $crawler;

    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * @return string
     * @throws RuntimeException if image container is not found
     */
    public function extractCaptchaImageUrl(): string
    {
        $image = $this->crawler->filter('#consultaCertificados img')
            ->reduce(static function(Crawler $image) {
                return str_contains($image->attr('src'), 'CAPTCHARenderer');
            })
            ->first();

        if ($image->count() === 0) {
            throw new RuntimeException('Unable to find "#consultaCertificados img" captcha image');
        }

        return $image->attr('src');
    }

    public function extractForm(): Form
    {
        return $this->crawler->selectButton('Buscar')->form();
    }

    public function getContents(): string
    {
        return $this->crawler->html();
    }
}

<?php

namespace Tests\Feature\Api;

use App\Http\Controllers\Api\CertificateBySerialNumber;
use Tests\TestCase;

/**
 * @see CertificateBySerialNumber
 */
final class CertificateBySerialNumberTest extends TestCase
{
    public function testAskingForKnownCertificateNumberReturnExpectedResult(): void
    {
        $certificateNumber = '00001000000404900196';

        $response = $this->getJson("/api/certificate/$certificateNumber");
        $response->assertStatus(200);

        $expectedResponse = [
            'serialNumber' => $certificateNumber,
            'legalName' => 'AEROVIAS DE MEXICO SA DE CV',
            'rfc' => 'AME880912I89',
            'validSince' => '2017-01-20T17:46:27+00:00',
            'validUntil' => '2021-01-20T17:47:07+00:00',
        ];
        $response->assertJson($expectedResponse, true);
    }

    public function providerAskingForInvalidCertificateNumber(): array
    {
        return [
            'non decimal values' => ['XXXXXXXXXXXXXXXXXXXX'],
            '19 decimals' => ['0000000000000000000'],
            '21 decimals' => ['000000000000000000000'],
        ];
    }

    /** @dataProvider providerAskingForInvalidCertificateNumber */
    public function testAskingForInvalidCertificateNumber(string $certificateNumber): void
    {
        $response = $this->getJson("/api/certificate/$certificateNumber");
        $response->assertStatus(404);
    }

    public function testAskingForNonExistentCertificateNumber(): void
    {
        $certificateNumber = '99999999999999999999';
        $response = $this->getJson("/api/certificate/$certificateNumber");
        $response->assertStatus(404);
    }
}

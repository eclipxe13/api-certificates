<?php

namespace Tests\Feature\Api;

use App\Http\Controllers\Api\CertificateDownloadBySerialNumber;
use Tests\TestCase;

/**
 * @see CertificateDownloadBySerialNumber
 */
final class CertificateDownloadBySerialNumberTest extends TestCase
{
    public function testAskingForKnownCertificateNumberReturnExpectedResult(): void
    {
        $certificateNumber = '00001000000404900196';
        $expectedFilename = "$certificateNumber.cer";
        $expectedFile = __DIR__ . '/' . $expectedFilename;

        $response = $this->get("/api/certificate/$certificateNumber/download");
        $response->assertStatus(200);
        $response->assertDownload($expectedFilename);
        $response->assertHeader('Content-Type', 'application/octet-stream');

        $this->assertEquals(
            md5_file($expectedFile),
            md5($response->streamedContent()),
            'Expected file has not the same md5 hash'
        );
    }

    public function providerAskingForInvalidCertificateNumber(): array
    {
        return [
            'non decimal values' => ['XXXXXXXXXXXXXXXXXXXX'],
            '19 decimals' => ['0000000000000000000'],
            '21 decimals' => ['000000000000000000000'],
        ];
    }

    /** @dataProvider providerAskingForInvalidCertificateNumber */
    public function testAskingForInvalidCertificateNumber(string $certificateNumber): void
    {
        $response = $this->get("/api/certificate/$certificateNumber/download");
        $response->assertStatus(404);
    }

    public function testAskingForNonExistentCertificateNumber(): void
    {
        $certificateNumber = '99999999999999999999';
        $response = $this->get("/api/certificate/$certificateNumber/download");
        $response->assertStatus(404);
    }
}

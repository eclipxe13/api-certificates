<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\Tests\SatCertificateVault;

use Eclipxe\ApiSatCertificates\SatCertificateVault\Scraper;
use Eclipxe\ApiSatCertificates\SatCertificateVault\SearchPage;
use Goutte\Client as Browser;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Tests\TestCase;

final class ScraperTest extends TestCase
{
    /**
     * @param MockResponse[] $responses
     * @return Scraper
     */
    public function createScraper(array $responses): Scraper
    {
        return new Scraper(new Browser(new MockHttpClient($responses)));
    }

    /** @return array<MockResponse[]> */
    public function providerInvalidResponses(): array
    {
        return [
            'response is 404' => [[new MockResponse('', ['http_code' => 404])]],
            'communication error' => [[]],
        ];
    }

    public function testObtainSearchPageWithExpectedPage(): void
    {
        $scraper = $this->createScraper([
            new MockResponse(file_get_contents(__DIR__ . '/_files/search_page_sample.html')),
        ]);

        $page = $scraper->obtainSearchPage();

        $this->assertStringContainsString(
            '<title>Recuperación de Certificados</title>',
            $page->getContents()
        );
    }

    /** @dataProvider providerInvalidResponses */
    public function testObtainSearchPageWithError(array $responses): void
    {
        $scraper = $this->createScraper($responses);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unable to get search page');
        $scraper->obtainSearchPage();
    }

    public function testDownloadImageWithExpectedContent(): void
    {
        $scrapFile = __DIR__ . '/_files/captcha-example.png';
        $scrapFileContents = file_get_contents($scrapFile);
        $scraper = $this->createScraper([
            new MockResponse($scrapFileContents),
        ]);

        $image = $scraper->downloadImage('https://example.com/');

        $this->assertStringEqualsFile($scrapFile, $image->getValue());
    }

    /** @dataProvider providerInvalidResponses */
    public function testDownloadImageWithError(array $responses): void
    {
        $scraper = $this->createScraper($responses);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unable to get captcha image');
        $scraper->downloadImage('https://example.com/');
    }

    public function testObtainResultsPageWithExpectedContent(): void
    {
        $searchPage = new SearchPage(new Crawler(
            file_get_contents(__DIR__ . '/_files/search_page_sample.html'),
            Scraper::URL_SEARCH_PAGE
        ));
        $rfc = 'AAAA010101AAA';
        $captcha = '123456';
        $scraper = $this->createScraper([
            new MockResponse(file_get_contents(__DIR__ . '/_files/results_page_sample.html')),
        ]);

        $page = $scraper->obtainResultsPage($searchPage, $rfc, $captcha);

        $this->assertStringContainsString(
            '<title>Recuperación de Certificados</title>',
            $page->getContents()
        );
    }

    /** @dataProvider providerInvalidResponses */
    public function testObtainResultsPageWithError(array $responses): void
    {
        $scraper = $this->createScraper($responses);
        /** @var SearchPage $searchPage */
        $searchPage = $this->mock(SearchPage::class);
        $rfc = 'AAAA010101AAA';
        $captcha = '123456';

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unable to get results page');
        $scraper->obtainResultsPage($searchPage, $rfc, $captcha);
    }
}

<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\Tests\SatCertificateVault;

use Eclipxe\ApiSatCertificates\SatCertificateVault\SearchPage;
use Eclipxe\ApiSatCertificates\SatCertificateVault\Scraper;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;
use Tests\TestCase;

final class FormPageTest extends TestCase
{
    private function createSearchPage(string $filepath): SearchPage
    {
        return new SearchPage(new Crawler(file_get_contents($filepath), Scraper::URL_SEARCH_PAGE));
    }

    public function testExtractCaptchaImageUrl(): void
    {
        $page = $this->createSearchPage(__DIR__ . '/_files/search_page_sample.html');
        $expectedUrl = '/RecuperacionDeCertificados/faces/myFacesExtensionResource'
            . '/org.apache.myfaces.custom.captcha.CAPTCHARenderer/16253341/'
            . '?captchaSessionKeyName=mySessionKeyName&dummyParameter=1626831289802';
        $this->assertSame($expectedUrl, $page->extractCaptchaImageUrl());
    }

    public function testExtractCaptchaImageUrlThrowsExceptionWhenNotFound(): void
    {
        $page = $this->createSearchPage(__DIR__ . '/_files/dummy.html');
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Unable to find "#consultaCertificados img" captcha image');
        $page->extractCaptchaImageUrl();
    }

    public function testExtractForm(): void
    {
        $page = $this->createSearchPage(__DIR__ . '/_files/search_page_sample.html');
        $form = $page->extractForm();
        $expectedFields = [
            'consultaCertificados:botonRFC',
            'consultaCertificados',
            'consultaCertificados:entradaRFC',
            'consultaCertificados:todos',
            'consultaCertificados:numeroDeSerie',
            'consultaCertificados:verCaptchaRFC',
            'javax.faces.ViewState',
        ];
        $this->assertEquals($expectedFields, array_keys($form->getValues()));
    }
}

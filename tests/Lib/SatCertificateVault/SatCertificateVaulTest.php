<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\Tests\SatCertificateVault;

use Eclipxe\ApiSatCertificates\SatCertificateVault\CertificateList;
use Eclipxe\ApiSatCertificates\SatCertificateVault\SatCertificateVault;
use Eclipxe\ApiSatCertificates\SatCertificateVault\CaptchaResolvers\LocalCaptchaResolver;
use Eclipxe\ApiSatCertificates\SatCertificateVault\Scraper;
use Tests\TestCase;

final class SatCertificateVaulTest extends TestCase
{
    public function testObtainCertificateCreatesList(): void
    {
        $rfc = 'AME880912I89';
        $resolver = new LocalCaptchaResolver();
        $scraper = new Scraper();

        $vault = new SatCertificateVault($rfc, $resolver, $scraper);
        $this->assertSame($rfc, $vault->getRfc());
        $this->assertSame($resolver, $vault->getResolver());
        $this->assertSame($scraper, $vault->getScraper());

        $list = $vault->obtainList();

        $this->assertInstanceOf(CertificateList::class, $list);
    }
}

<?php

declare(strict_types=1);

namespace Eclipxe\ApiSatCertificates\Tests\CertificateDownloader;

use Eclipxe\ApiSatCertificates\CertificateDownloader\CertificateDownloader;
use Eclipxe\ApiSatCertificates\CertificateDownloader\HttpDownloaderInterface;
use Exception;
use Illuminate\Cache\ArrayStore;
use Illuminate\Cache\Repository as Cache;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;

final class CertificateDownloaderTest extends TestCase
{
    private function createArrayCache(): CacheInterface
    {
        return new Cache(new ArrayStore());
    }

    public function testRetrieveStoresInCacheIfNotPresent(): void
    {
        $certificateNumber = '1234567890';
        $expectedContent = 'foo-bar';

        $cache = $this->createArrayCache();

        /** @var HttpDownloaderInterface&MockObject $httpDownloader */
        $httpDownloader = $this->createMock(HttpDownloaderInterface::class);
        $httpDownloader->method('retrieve')->willReturn($expectedContent);

        $cerDownloader = new CertificateDownloader($cache, $httpDownloader);
        $url = $cerDownloader->buildCertificateUrl($certificateNumber);

        $this->assertFalse($cache->has($url), 'The url must not exists in the cache');

        $cerDownloader->retrieve($certificateNumber);
        $this->assertSame($expectedContent, $cerDownloader->retrieve($certificateNumber));

        $this->assertTrue($cache->has($url), 'The url must exist after download');
        $this->assertSame($expectedContent, $cache->get($url), 'The url must exist after download');
    }

    public function testRetrieveRetrievesFromCacheIfPresent(): void
    {
        $certificateNumber = '1234567890';
        $expectedContent = 'foo-bar';

        $cache = $this->createArrayCache();

        /** @var HttpDownloaderInterface&MockObject $httpDownloader */
        $httpDownloader = $this->createMock(HttpDownloaderInterface::class);
        $httpDownloader->method('retrieve')->willThrowException(
            new Exception('retrieve method should not be called')
        );

        $cerDownloader = new CertificateDownloader($cache, $httpDownloader);
        $url = $cerDownloader->buildCertificateUrl($certificateNumber);
        $cache->set($url, $expectedContent);

        // retrieve the first time
        $cerDownloader->retrieve($certificateNumber);
        $this->assertSame($expectedContent, $cerDownloader->retrieve($certificateNumber));
    }
}

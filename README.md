# API SAT Certificates

Una API JSON para descargar y comprobar la validez de un certificado en un determinado momento en el tiempo.

## Endpoints

### Información del certificado

```
GET /api/certificate/{serial-number}
```

### Descarga del certificado en formato DER

```
GET /api/certificate/{serial-number}/download
```

### Listado de certificados por un RFC específico

```
GET /api/rfc/{rfc}
```

Devuelve un arreglo de objetos con la siguiente estructura:

```json
[
    {
        "serialNumber": "000000000000000001",
        "status": "Vigente",
        "type": "CSD",
        "validSince": "2018-07-21T04:18:25-05:00",
        "validUntil": "2020-07-21T04:18:25-05:00"
    }
]
```

- `serialNumber` número de serie.
- `status` puede tener el valor `Vigente`, `Revocado` o `Caduco`.
- `type` puede tener el valor `CSD` o `FIEL`.
- `validSince` fecha de inicio de vigencia.
- `validUntil` fecha de caducidad o expiración.

### Rutas pendientes de desarrollo

```
Determina si el certificado es válido en este momento
GET /api/certificate/{serial-number}/valid/

Determina si el certificado es válido en esa fecha en el tiempo
GET /api/certificate/{serial-number}/valid/{timestamp}
```

Esto va a requerir:
- Un scraper para la página del SAT
- Un repositorio de certificados (datos y archivo)
- Usar cache para no consumir el scraper en caso de no necesitarlo

## License

The `eclipxe/api-sat-certificates` project is copyright © [Carlos C Soto](https://eclipxe.com.mx/)
and licensed for use under the MIT License (MIT). Please see [LICENSE][] for more information.

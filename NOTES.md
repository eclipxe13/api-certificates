# Notas

## TODO

Nos quedamos creando el descargador de certificados, faltan las siguientes tareas:

- Convertir el HTML del resultado de la consulta a un listado.

- Implementar la resolución de captchas con varios clientes.

- Si se usa la librería de resolución de captchas tipo imagen es probable que sea mejor cambiar
  las dependencias de `symfony/http-client` a `guzzlehttp/guzzle`. O <https://github.com/JustSteveKing/http-slim>

## Requisitos

- RFC y Resolvedor de captchas

## Salida

- Listado de certificados disponibles

## Proceso

- Nos tenemos que conectar al sitio del SAT
  <https://portalsat.plataforma.sat.gob.mx/RecuperacionDeCertificados/faces/consultaCertificados.xhtml>

```
GET https://portalsat.plataforma.sat.gob.mx/RecuperacionDeCertificados/faces/consultaCertificados.xhtml

<< captcha url

GET /RecuperacionDeCertificados/faces/myFacesExtensionResource/org.apache.myfaces.custom.captcha.CAPTCHARenderer/16253341/?captchaSessionKeyName=mySessionKeyName&amp;dummyParameter=1626831289802
```

- Resolver el catpcha

- Enviar el formulario `#consultaCertificados`
  con RFC `consultaCertificados:entradaRFC`
  y CAPTCHA `consultaCertificados:verCaptchaRFC`

```
POST https://portalsat.plataforma.sat.gob.mx/RecuperacionDeCertificados/faces/consultaCertificados.xhtml
    'consultaCertificados:botonRFC' => 'Buscar'
    'consultaCertificados' => 'consultaCertificados'
    'consultaCertificados:entradaRFC' => ''
    'consultaCertificados:todos' => 'todos'
    'consultaCertificados:numeroDeSerie' => ''
    'consultaCertificados:verCaptchaRFC' => ''
    'javax.faces.ViewState' => '488952042765655369:-933713138402686277'
```

- Procesar el listado para extraer los datos

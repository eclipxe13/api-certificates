<?php

namespace App\Providers;

use Eclipxe\ApiSatCertificates\CertificateDownloader\CertificateDownloader;
use Eclipxe\ApiSatCertificates\CertificateDownloader\HttpDownloaderInterface;
use Eclipxe\ApiSatCertificates\CertificateDownloader\PhpStreamDownloader;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind(HttpDownloaderInterface::class, PhpStreamDownloader::class);
        $this->app->bind(CertificateDownloader::class, function () {
            return new CertificateDownloader(
                Cache::store('certificates'),
                new PhpStreamDownloader()
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use Eclipxe\ApiSatCertificates\CertificateDownloader\CertificateDownloader;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class CertificateDownloadBySerialNumber
{
    /** @var CertificateDownloader */
    private $downloader;

    public function __construct(CertificateDownloader $downloader)
    {
        $this->downloader = $downloader;
    }

    public function __invoke(Request $request, string $serialNumber): StreamedResponse
    {
        // obtener el contenido del certificado
        try {
            $certificateContents = $this->downloader->retrieve($serialNumber);
        } catch (Throwable $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }

        return response()->streamDownload(
            function () use ($certificateContents) {
                echo $certificateContents;
            },
            $serialNumber . '.cer',
            ['Content-Type' => 'application/octet-stream']
        );
    }
}

<?php

namespace App\Http\Controllers\Api;

use DateTimeZone;
use Eclipxe\ApiSatCertificates\CertificateDownloader\CertificateDownloader;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PhpCfdi\Credentials\Certificate;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class CertificateBySerialNumber
{
    /** @var CertificateDownloader */
    private $downloader;

    public function __construct(CertificateDownloader $downloader)
    {
        $this->downloader = $downloader;
    }

    public function __invoke(Request $request, string $serialNumber): JsonResponse
    {
        // obtener el contenido del certificado
        try {
            $certificateContents = $this->downloader->retrieve($serialNumber);
        } catch (Throwable $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }

        // creamos un objeto de certificado
        $certificate = new Certificate($certificateContents);

        // retornamos sus propiedades
        return new JsonResponse([
            'serialNumber' => $certificate->serialNumber()->bytes(),
            'legalName' => $certificate->legalName(),
            'rfc' => $certificate->rfc(),
            'validSince' => $certificate->validFromDateTime()->setTimeZone(new DateTimeZone('GMT'))->format('c'),
            'validUntil' => $certificate->validToDateTime()->setTimeZone(new DateTimeZone('GMT'))->format('c'),
        ]);
    }
}
